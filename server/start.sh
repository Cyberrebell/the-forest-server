#!/usr/bin/env bash
set -e
xvfb-run wine64 ./TheForestDedicatedServer.exe -batchmode -nographics -dedicated -savefolderpath ./saves -configfilepath ./config.cfg > /dev/null
